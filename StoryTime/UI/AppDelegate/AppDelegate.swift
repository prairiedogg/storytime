import Foundation
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
   
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        self.window = UIWindow()
        
        if let window = self.window {
            window.frame = UIScreen.mainScreen().bounds
            window.backgroundColor = UIColor.whiteColor()


            if let isTest: AnyClass = NSClassFromString("XCTestCase") {
                window.rootViewController = UIViewController()
            }
            else {
                let session = NSURLSession.sharedSession()
                let mainQueue = NSOperationQueue.mainQueue()
                let storyRepository = StoryRepository(session: session, operationQueue: mainQueue)
                window.rootViewController = StoryBucketController(nibName: "StoryBucketController", bundle: nil, storyRepository: storyRepository)
            }
            window.makeKeyAndVisible()
        }
        
        return true
    }
}

