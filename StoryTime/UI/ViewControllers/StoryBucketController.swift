import UIKit


class StoryBucketController: UIViewController {
    @IBOutlet weak var storyLabel: UILabel!
    let storyRepository : StoryRepository?

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?, storyRepository: StoryRepository) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.storyRepository = storyRepository
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if let storyRepository = self.storyRepository {
            storyRepository.fetchAllCompletedOnePointStories({ (numberOfCompletedOnePointStories) -> (Void) in
                let text = String(format: "The project has %d completed 1 point stories", numberOfCompletedOnePointStories)
                self.storyLabel.text = text
            })
        }
    }
}
