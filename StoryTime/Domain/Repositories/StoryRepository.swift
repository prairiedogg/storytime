import Foundation

class StoryRepository {
    let session : NSURLSession
    let operationQueue : NSOperationQueue
    
    init (session: NSURLSession, operationQueue: NSOperationQueue) {
        self.session = session
        self.operationQueue = operationQueue
    }
    
    func fetchAllCompletedOnePointStories(completionBlock: (Int) -> (Void)) -> NSURLSessionDataTask {
        let projectID = "YOUR_PROJECT_ID_HERE"
        let path = String(format: "https://www.pivotaltracker.com/services/v5/projects/%@/stories", projectID)
        let url = NSURL(string: path)!
        
        let request = NSMutableURLRequest(URL: url)
        let authToken = "YOUR_AUTH_TOKEN_HERE"
        request.setValue(authToken, forHTTPHeaderField: "X-TrackerToken")
        
        let task = session.dataTaskWithRequest(request, completionHandler: {(data : NSData!, response : NSURLResponse!, error : NSError!) in
            self.operationQueue.addOperationWithBlock({ () -> Void in
                var dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                var stories : Array<NSDictionary> = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as Array<NSDictionary>
                var numOfAcceptedOnePointStories : Int = 0
                for story in stories {
                    var currentState : String = story["current_state"] as String
                    if let estimate: Int = story.objectForKey("estimate") as? Int {
                        if(currentState == "accepted" && estimate == 1) {
                            numOfAcceptedOnePointStories += 1
                        }
                    }
                }
                
                completionBlock(numOfAcceptedOnePointStories)
            })
        });

        task.resume()
        return task
    }
}