import UIKit
import XCTest

class AppDelegatesMainWindow: XCTestCase {
    var subject: UIWindow?
    
    
    override func setUp() {
        super.setUp()
        let appDelegate = AppDelegate()
        let application = UIApplication.sharedApplication()
        appDelegate.application(application, didFinishLaunchingWithOptions: nil)
        self.subject = appDelegate.window
    }
    
    func testExists() {
        XCTAssertNotNil(self.subject)
    }
    
    func testHasCorrectSize() {
        XCTAssertNotNil(self.subject!.bounds.size.width)
        var expectedSize : CGSize = UIScreen.mainScreen().bounds.size
        XCTAssertEqual(self.subject!.bounds.size, expectedSize)
    }
    
    func testIsKeyWindow(){
        XCTAssertTrue(self.subject!.keyWindow)
    }
    
    func testIsVisible(){
        XCTAssertTrue(!self.subject!.hidden)
    }
}