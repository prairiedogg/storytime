import UIKit
import XCTest

class StoryBucketControllerTests: XCTestCase {
    var subject: StoryBucketController!
    let storyRepository: FakeStoryRepository = FakeStoryRepository(session: FakeURLSession(), operationQueue: FakeOperationQueue())
    
    override func setUp() {
        super.setUp()
        self.subject = StoryBucketController(nibName: "StoryBucketController", bundle: nil, storyRepository: storyRepository)
    }
    
    func testItShowsNumberOfAcceptedOnePointStories(){
        XCTAssertNotNil(subject.view)
        subject.viewWillAppear(true)
        subject.viewDidAppear(true)
        
        storyRepository.acceptedOnePointStoriesBlock(3)
        
        XCTAssertEqual(self.subject.storyLabel!.text!, "The project has 3 completed 1 point stories")
    }
}