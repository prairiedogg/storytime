import UIKit
import XCTest

class StoryRepositoryTests: XCTestCase {
    var subject: StoryRepository!
    var session: FakeURLSession!
    
    override func setUp() {
        super.setUp()
        session = FakeURLSession()
        let queue = FakeOperationQueue()
        
        self.subject = StoryRepository(session: session, operationQueue: queue)
    }
    
    func testShouldFetchAllCompletedOnePointStoriesFromTheInternets() {
        var numberOfStories : Int?
        let dataTask = self.subject.fetchAllCompletedOnePointStories { (numberOfOnePointStories: Int) -> (Void) in
            numberOfStories = numberOfOnePointStories
        } as FakeDataTask
        
        let plainString = "[{\"current_state\": \"accepted\", \"estimate\": 1}, {\"current_state\": \"accepted\", \"estimate\": 3}]"
        let plainData = plainString.dataUsingEncoding(NSUTF8StringEncoding)
        dataTask.completionHandler(plainData, nil, nil)

        XCTAssertEqual(numberOfStories!, 1)
    }
}
