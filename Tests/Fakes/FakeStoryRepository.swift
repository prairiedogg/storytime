import Foundation


class FakeStoryRepository : StoryRepository {
    var acceptedOnePointStoriesBlock :((Int) -> (Void))!

    override func fetchAllCompletedOnePointStories(completionBlock: (Int) -> (Void)) -> NSURLSessionDataTask {
        self.acceptedOnePointStoriesBlock = completionBlock
        return NSURLSessionDataTask()
    }
    
}