import Foundation


class FakeURLSession : NSURLSession {
    var lastRequest :  NSURLRequest?
    var lastCompletionHandler : ((NSData!, NSURLResponse!, NSError!) -> Void)?
    
    override func dataTaskWithRequest(request: NSURLRequest, completionHandler: ((NSData!, NSURLResponse!, NSError!) -> Void)?) -> NSURLSessionDataTask {
        self.lastRequest = request
        return FakeDataTask(completionHandler: completionHandler!)
    }
}