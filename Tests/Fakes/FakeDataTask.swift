import Foundation


class FakeDataTask: NSURLSessionDataTask {
    let completionHandler : ((NSData!, NSURLResponse!, NSError!) -> Void)
    
    init(completionHandler: ((NSData!, NSURLResponse!, NSError!) -> Void)) {
        self.completionHandler = completionHandler
    }
}