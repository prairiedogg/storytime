import Foundation

class FakeOperationQueue : NSOperationQueue {
    override func addOperationWithBlock(block: () -> Void) {
        block()
    }
}